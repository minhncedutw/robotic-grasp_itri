# Install OpenNI driver #

### How do I get set up? ###
1. Download [OpenNI driver](http://dl.orbbec3d.com/dist/openni2/OpenNI_2.3.0.43.zip)
Ubuntu: Run `download.sh`
2. Extract driver to driver folder like this:
![](/driver/folder_tree.PNG)

> Each operating system need its own suitable driver just extract exact driver to the 'driver' folder here.